"use strict";

class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  set name(name) {
    this._name = name;
  }

  get name() {
    return this._name;
  }

  set age(age) {
    this._age = age;
  }

  get age() {
    return this._age;
  }

  set salary(salary) {
    this._salary = salary;
  }

  get salary() {
    return this._salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }

  get salary() {
    return this._salary;
  }

  set salary(salary) {
    this._salary = salary * 3;
  }
}

const user1 = new Programmer("Ivan", "45", 3000, ["js", "PHP", "c++"]);
const user2 = new Programmer("Ivan", "35", 2000, ["js", "PHP"]);
const user3 = new Programmer("Ivan", "25", 1000, ["js"]);
console.log("user1", user1);
console.log("user2", user2);
console.log("user3", user3);
